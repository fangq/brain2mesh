Source: brain2mesh
Maintainer: Qianqian Fang <fangqq@gmail.com>
Section: science
Priority: optional
Standards-Version: 4.5.0
Build-Depends: debhelper (>= 8.1.3~), debhelper-compat (= 12), dh-octave
Homepage: https://mcx.space/brain2mesh


Package: octave-brain2mesh
Section: science
Architecture: all
Depends: octave-iso2mesh, ${octave:Depends}, ${misc:Depends}
Description: fully automated high-quality brain mesh generator for Octave
 The Brain2Mesh toolbox provides a streamlined matlab function to convert
 a segmented brain volumes and surfaces into a high-quality multi-layered
 tetrahedral brain/full head mesh. Typical inputs include segmentation
 outputs from SPM, FreeSurfer, FSL etc. This tool does not handle the
 segmentation of MRI scans, but examples of how commonly encountered
 segmented datasets can be used to create meshes can be found in the 
 documentation folder.


Package: matlab-brain2mesh
Section: contrib/science
Architecture: all
Depends: octave-iso2mesh, matlab-support, ${misc:Depends}
Description: fully automated high-quality brain mesh generator for MATLAB
 This package contains the MATLAB copy of the Brain2Mesh toolbox for
 automated high-quality brain mesh generation using labeled segmentations
 or tissue probablilistic maps (TPM) segmentations.
 .
 The details of Brain2Mesh can be found in the paper "Anh Phong Tran, 
 Shijie Yan, Qianqian Fang*, (2020) "Improving model-based fNIRS analysis 
 using mesh-based anatomical and light-transport models," Neurophotonics, 
 7(1), 015008"
